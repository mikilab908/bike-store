# Bike Store

Bike Store is a single-page application built in React. The main feature of this application is that all products are fetched via an external API using Axios. Built with: HTML, CSS, Bootstrap, React. 
 