import './App.css';
import Filter from './components/Filter';
import Header from './components/Header';
import Footer from './components/Footer';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-10 offset-md-1 content-bg'>
            <Header />
            <Filter />
            <Footer />
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
