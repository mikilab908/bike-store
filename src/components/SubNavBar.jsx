import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';

function SubNavBar() {
  return (
    <>
      <div className='row'>
        <div className='col-md-12 text-left title'>
          <div className='hr'></div>
          <h1>Bikes</h1>
          <div className='hr'></div>
        </div>
      </div>
    </>
  );
}

export default SubNavBar;
