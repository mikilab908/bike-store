import React, { Component } from 'react';
import axios from 'axios';
import Card from './Card';
import SideBar from './SideBar';
import 'bootstrap/dist/css/bootstrap.css';

class Filter extends Component {
  state = {
    baseData: [],
    data: [],
    genders: [],
    brands: [],
    targetValue: '',
  };

  componentDidMount() {
    axios('https://json-project3.herokuapp.com/products').then((res) => {
      const baseData = res.data;
      const data = res.data;
      const genders = [...new Set(data.map((item) => item.gender))];
      const brands = [...new Set(data.map((item) => item.brand))];

      this.setState({
        baseData,
        data,
        genders,
        brands,
      });
    });
  }

  filterCards = (arr, tag) => {
    this.setState({
      data: arr,
      targetValue: tag ? tag : '',
    });
  };

  render() {
    const { baseData, data, targetValue, genders, brands } = this.state;

    return (
      <>
        <section className='body'>
          <div className='row'>
            <div className='col-md-3 side-bar'>
              <SideBar
                onHandleClick={this.filterCards}
                baseData={baseData}
                data={data}
                targetValue={targetValue}
                genders={genders}
                brands={brands}
              />
            </div>
            <div className='col-md-9 main bike-showcase px-0'>
              {data.map((card, idx) => {
                const { image, name, price } = card;

                return (
                  <Card image={image} name={name} price={price} key={idx} />
                );
              })}
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default Filter;
