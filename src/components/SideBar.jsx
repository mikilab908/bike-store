import React, { Component } from 'react';

export class SideBar extends Component {
  render() {
    const {
      baseData,
      onHandleClick,
      targetValue,
      genders,
      brands,
    } = this.props;
    return (
      <>
        <div className='row'>
          <div className='col-md-12'>
            <div className='h3'>Filter by:</div>
            <ul className='show-all'>
              <li
                className={targetValue === '' ? 'active' : ''}
                onClick={() => onHandleClick(baseData, undefined)}
              >
                <a className='all-li'>
                  <span className='text'>Show all</span>
                  <span className='badge badge-pill'>{baseData.length}</span>
                </a>
              </li>
            </ul>
            <div className='hr'></div>
          </div>
        </div>

        <div className='row'>
          <div className='col-md-12'>
            <div className='h4'>Gender</div>
            <ul className='gender-links'>
              {genders.map((gender, idx) => {
                let arrLength = baseData.filter((el) => el.gender === gender);
                return (
                  <li
                    key={idx}
                    className={targetValue === gender ? 'active' : ''}
                    onClick={() => onHandleClick(arrLength, gender)}
                  >
                    <a className='all-li'>
                      <span className='text'>
                        {gender === 'MALE' ? 'Male' : 'Female'}
                      </span>
                      <span className='badge badge-pill'>
                        {arrLength.length}
                      </span>
                    </a>
                  </li>
                );
              })}
            </ul>
            <div className='hr'></div>
          </div>
        </div>

        <div className='row'>
          <div className='col-md-12'>
            <div className='h4'>Brand</div>
            <ul className='brand-links'>
              {brands.map((brand, idx) => {
                let arrLength = baseData.filter((el) => el.brand === brand);
                return (
                  <li
                    key={idx}
                    className={targetValue === brand ? 'active' : ''}
                    onClick={() => onHandleClick(arrLength, brand)}
                  >
                    <a className='all-li'>
                      <span className='text'>{brand}</span>
                      <span className='badge badge-pill'>
                        {arrLength.length}
                      </span>
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </>
    );
  }
}

export default SideBar;
