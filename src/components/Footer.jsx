import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebookF,
  faInstagram,
  faTwitter,
  faLinkedinIn,
} from '@fortawesome/free-brands-svg-icons';

export class Footer extends Component {
  state = {
    social: [faFacebookF, faInstagram, faTwitter, faLinkedinIn],
    events: [
      'Enter Now',
      'Event Info',
      'Course Maps',
      'Race Pack',
      'Results',
      'FAQs',
      'Am I Registered?',
    ],
    register: [
      'Volunteers',
      'Gallery',
      'Press',
      'Results',
      'Privacy Policy',
      'Service Plus',
      'Contacts',
    ],
    schedule: [
      'Gallery',
      'About',
      'Videos',
      'Results',
      'FAQs',
      'Results',
      'Volunteers',
    ],
  };

  render() {
    return (
      <div>
        <footer>
          <div className='row'>
            <div className='col-md-12 footer-bg'>
              <div className='row footer-padding'>
                <div className='col-md-3'>
                  <div className='h4'>Social share</div>
                  <ul className='social'>
                    {this.state.social.map((icon, idx) => (
                      <li key={idx}>
                        <a>
                          <i>
                            <FontAwesomeIcon icon={icon} />
                          </i>
                        </a>
                      </li>
                    ))}
                  </ul>
                </div>

                <div className='col-md-3'>
                  <div className='footer-border'>
                    <div className='h4'>Event info</div>
                    <ul>
                      {this.state.events.map((el, idx) => (
                        <li key={idx} className='all-li'>
                          <a>{el}</a>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>

                <div className='col-md-3'>
                  <div className='footer-border'>
                    <div className='h4'>Registration</div>
                    <ul>
                      {this.state.register.map((el, idx) => (
                        <li key={idx} className='all-li'>
                          <a>{el}</a>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>

                <div className='col-md-3'>
                  <div className='footer-border'>
                    <div className='h4'>Schedule</div>
                    <ul>
                      {this.state.schedule.map((el, idx) => (
                        <li key={idx} className='all-li'>
                          <a>{el}</a>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

export default Footer;
