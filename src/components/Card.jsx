import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';

function SingleCard({ image, name, price, idx }) {
  return (
    <>
      <div className='col-sm-6 col-md-4' key={idx}>
        <div className='thumbnail'>
          <div className='image'>
            <img
              src={process.env.PUBLIC_URL + `/img/${image}.png`}
              className='img img-responsive'
              alt='bikes'
            />
          </div>
          <div className='caption'>
            <div className='h5 title'>{name}</div>
            <p className='price'>{price} $</p>
          </div>
        </div>
      </div>
    </>
  );
}

export default SingleCard;
