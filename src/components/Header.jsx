import React, { Component } from 'react';
import NavBar from './NavBar';
import SubNavBar from './SubNavBar';

class Header extends Component {
  render() {
    return (
      <>
        <NavBar />
        <SubNavBar />
      </>
    );
  }
}

export default Header;
