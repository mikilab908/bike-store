import React, { Component } from 'react';
import logo from '../logo.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import 'bootstrap/dist/css/bootstrap.css';

class NavBar extends Component {
  state = {
    links: [
      'HOME',
      'BIKES',
      'GEAR',
      'PARTS',
      'TIRES',
      'SERVICE - INFO',
      'CATALOGUES',
      'CONTACT',
    ],
    icons: [faSearch, faShoppingBag],
  };

  render() {
    const { links, icons } = this.state;

    return (
      <>
        <div className='row'>
          <div className='col-md-12 navigation-menu'>
            <div className='logo-wrap'>
              <a className='' href='#'>
                <img src={logo} className='logo' alt='' />
              </a>
            </div>
            <div>
              <ul className='links'>
                {links.map((link, idx) => (
                  <li key={idx}>
                    <a href='#'>{link}</a>
                  </li>
                ))}
              </ul>
            </div>
            <div>
              <ul className='nav-icons'>
                {icons.map((icon, idx) => (
                  <li key={idx}>
                    <a href='#'>
                      <i>
                        <FontAwesomeIcon icon={icon} />
                      </i>
                    </a>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default NavBar;
